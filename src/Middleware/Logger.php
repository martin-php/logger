<?php

namespace Martin\Logger\Middleware;

use Closure;
use \Martin\Logger\Processor\IntrospectionFileNameProcessor;
use \Monolog\Processor\IntrospectionProcessor;

class Logger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logger = \Log::getLogger();

        // TODO: get headers for user client app-version

        $logger->pushProcessor(new IntrospectionFileNameProcessor(app_path()));
        $logger->pushProcessor(new IntrospectionProcessor(null, ['Illuminate\\']));

        $dateFormat = "y-m-d H:i:s";
        $output     = "%datetime% [%level_name%] %message% %context% [%extra.file%@%extra.function%:%extra.line%]\n"; // [$user|$client|$appVersion]\n";
        $formatter  = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true, true);

        $fileStream = new \Monolog\Handler\StreamHandler(storage_path('/logs/laravel.log'));
        $fileStream->setFormatter($formatter);
        $logger->pushHandler($fileStream);

        return $next($request);
    }
}

// $logger = \Log::getLogger();

// // TODO: get headers

// // $logger->pushProcessor(new \Monolog\Processor\PsrLogMessageProcessor);
// $logger->pushProcessor(new \Martin\Logger\Processor\IntrospectionFileNameProcessor(app_path()));
// $logger->pushProcessor(new IntrospectionProcessor(null, ['Illuminate\\']));
// // $logger->pushProcessor(
// //     new IntrospectionProcessor(
// //         [
// //             \Monolog\Logger::DEBUG,
// //             [
// //                 'Monolog\\',
// //                 'Illuminate\\',
// //             ],
// //         ]
// //     )
// // );

// $dateFormat = "y-m-d H:i:s";
// $output     = "%datetime% [%level_name%] %message% %context% [%extra.file%@%extra.function%:%extra.line%]\n"; // [$user|$client|$appVersion]\n";
// $formatter  = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true, true);

// $fileStream = new \Monolog\Handler\StreamHandler(storage_path('/logs/laravel.log'));
// $fileStream->setFormatter($formatter);
// $logger->pushHandler($fileStream);
// // $fileStream->setFormatter($formatter);
// // $fingersCrossed = new Monolog\Handler\FingersCrossedHandler($fileStream, $settings['level']);

// // $logger->pushHandler($fingersCrossed);
// // $logger->pushHandler(new Monolog\Handler\FirePHPHandler());
// // Monolog\ErrorHandler::register($logger);

// return $next($request);
