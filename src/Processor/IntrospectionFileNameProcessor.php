<?php
namespace Martin\Logger\Processor;

use Monolog\Logger;

/**
 * abbreviate IntrospectionProcessors filpath to only contain path within the project
 * pass root folder name to constructor
 */
class IntrospectionFileNameProcessor
{
    private $folderName;

    /**
     * constructor
     *
     * @param String $folderName -> projects root folder
     */
    public function __construct(String $folderName)
    {
        $this->folderName = $folderName;
    }

    public function __invoke(array $record)
    {
        if (isset($record['extra']['file'])) {
            $record['extra']['file'] = \str_replace('.php', '', $record['extra']['file']);
            $record['extra']['file'] = \str_replace(
                $this->folderName,
                '',
                \strstr($record['extra']['file'], $this->folderName)
            );
        }

        return $record;
    }
}
